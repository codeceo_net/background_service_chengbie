import 'package:flutter/foundation.dart';

import 'counter.dart';

class CounterService {
  //当你需要构造函数不是每次都创建一个新的对象时，使用factory关键字。
  factory CounterService.instance() => _instance;

  CounterService._internal();

  static final _instance = CounterService._internal();

  final _counter = Counter();

  ValueListenable<int> get count => _counter.count;

  void startCounting() {
    Stream.periodic(Duration(seconds: 1)).listen((_) {
      _counter.increment();
      print('Counter incremented: ${_counter.count.value}');
    });
  }
}
