import 'package:flutter/cupertino.dart';

import 'counter_service.dart';

void backgroundMain() {
  // 首先调用它很重要
  //任何特定于 Flutter 的东西都不起作用（即方法通道）。
  WidgetsFlutterBinding.ensureInitialized();
  // 以下服务与=在应用程序的 main 方法中启动的服务相同。
  // 通过这种方式，重用了相同的代码。
  CounterService.instance().startCounting();
}
