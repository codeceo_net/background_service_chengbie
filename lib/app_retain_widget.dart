import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

//如果我们的应用程序正在执行一些 Dart 代码以保持与 websocket 服务器的连接，
//那么当用户按下后退按钮时，代码将终止

//我们在按下后退按钮时进行拦截，并告诉 Android 本机代码调用moveTaskToBack.
// 这会将 FlutterActivity到后台而不是破坏它。

class AppRetainWidget extends StatelessWidget {
  const AppRetainWidget({Key key, this.child}) : super(key: key);

  final Widget child;

  final _channel = const MethodChannel('com.example/app_retain');

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (Platform.isAndroid) {
          if (Navigator.of(context).canPop()) {
            return true;
          } else {
            _channel.invokeMethod('sendToBackground');
            return false;
          }
        } else {
          return true;
        }
      },
      child: child,
    );
  }
}
