# background_service_chengbie

【如何让 Flutter 应用程序在 Android 的后台运行】
这是一个示例项目，讨论了一些保持 Flutter 应用程序运行的技术。

【应用功能】
这是一个简单的 Flutter 应用程序，
当它打开时，它会显示一个带有计数器的屏幕，该计数器每秒递增。

有趣的是计数器永远不会停止计数。
无论您是从最近的应用程序中删除该应用程序，还是 Android 系统都会终止该进程以回收更多内存。
该应用程序始终从任何情况中恢复并不断计数。
如果您想停止它，只需卸载该应用程序。

【这些是这个项目用来保持 Flutter 运行的技术】

    moveTaskToBack按下后退按钮时使用生存。
    创建前台服务以提高进程优先级。
    在FlutterActivity销毁时启动和停止新的 Isolate 以运行 Dart 后台代码。

【如何运行项目】

    按照入门教程安装 Flutter
    启动和安卓模拟器
    从终端运行应用程序 flutter run

